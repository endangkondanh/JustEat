package kaka_.hybi.justeat;

import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class gameView extends AppCompatActivity implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener{
    private TextView scoreLabel, nyawaMC;
    private ImageView mainCharacter, food_a, food_a1, food_s, food_ss, toxic_a, toxic_s;

    //position
    private float mainCharacterX;
    private float mainCharacterY;
    private float food_aX;
    private float food_aY;
    private float food_a1X;
    private float food_a1Y;
    private float food_sX;
    private float food_sY;
    private float food_ssX;
    private float food_ssY;
    private float toxic_aX;
    private float toxic_aY;
    private float toxic_sX;
    private float toxic_sY;

    //speed
    private int mainCharacterSpeed, fallSpeedFood, fallSpeedToxic ;

    //score
    private int score = 0;
    private int nyawa = 9;

    //size
    private int screenWidth,screenHeight ;

    //Intialize class
    private Handler handler = new Handler();
    private Timer timer = new Timer();
    private SoundPlayer sound;

    //status check
    private boolean action_flag = false;
    private  boolean action_jump = false;
    private GestureDetectorCompat gDetector;
    MediaPlayer backSound;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_view);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        backSound = MediaPlayer.create(this, R.raw.backsound1);
        sound = new SoundPlayer(this);

        this.gDetector = new GestureDetectorCompat(this, this);
        gDetector.setOnDoubleTapListener(this);

        scoreLabel = findViewById(R.id.scoreLabel);
        nyawaMC = findViewById(R.id.nyawaMC);
        mainCharacter = findViewById(R.id.mainCharacter);
        food_a = findViewById(R.id.food_a);
        food_a1 = findViewById(R.id.food_a1);
        food_s = findViewById(R.id.food_s);
        food_ss = findViewById(R.id.food_ss);
        toxic_a = findViewById(R.id.toxic_a);
        toxic_s = findViewById(R.id.toxic_s);

        //get screen size
        WindowManager wm = getWindowManager();
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        screenWidth = size.x;
        screenHeight = size.y;

        mainCharacterSpeed = Math.round(screenWidth / 100F);
        fallSpeedFood = Math.round(screenHeight / 150F);
        fallSpeedToxic = Math.round(screenHeight / 100F);

        mainCharacterY = mainCharacter.getY();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        hitCheck();
                        counter();
                        character();
                        gameOver();
                        backSound.start();
                    }
                });
            }
        }, 0, 20);
    }

    public void hitCheck() {
        //food_a
        food_a.setX(food_aX);
        food_a.setY(food_aY);
        food_aY += fallSpeedFood;
        if (food_aY > screenWidth) {
            food_aX = (int) Math.floor(Math.random() * (screenWidth - food_a.getWidth()));
            food_aY = 50;
        }

        food_s.setX(food_sX);
        food_s.setY(food_sY);
        food_sY += fallSpeedFood;
        if (food_sY > screenWidth) {
            food_sX = (int) Math.floor(Math.random() * (screenWidth - food_a.getWidth()));
            food_sY = 250;
        }

        food_a1.setX(food_a1X);
        food_a1.setY(food_a1Y);
        food_a1Y += fallSpeedFood;
        if (food_a1Y > screenWidth) {
            food_a1X = (int) Math.floor(Math.random() * (screenWidth - food_a.getWidth()));
            food_a1Y = 50;
        }

        food_ss.setX(food_ssX);
        food_ss.setY(food_ssY);
        food_ssY += fallSpeedFood;
        if (food_ssY > screenWidth) {
            food_ssX = (int) Math.floor(Math.random() * (screenWidth - food_a.getWidth()));
            food_ssY = 500;
        }

        //toxic_a
        toxic_a.setX(toxic_aX);
        toxic_a.setY(toxic_aY);
        toxic_aY += fallSpeedToxic;
        if (toxic_a.getY() > screenWidth) {
            toxic_aX = (int) Math.floor(Math.random() * (screenWidth - toxic_a.getWidth()));
            toxic_aY = 1;
        }

        toxic_s.setX(toxic_sX);
        toxic_s.setY(toxic_sY);
        toxic_sY += fallSpeedToxic;
        if (toxic_s.getY() > screenWidth) {
            toxic_sX = (int) Math.floor(Math.random() * (screenWidth - toxic_a.getWidth()));
            toxic_sY = 20;
        }
    }

    public void counter() {
        nyawaMC.setText("nyawa: " + nyawa);
        scoreLabel.setText("Score : " + score);

        //food
        if (eat(food_aX, food_aY)) {
            score += 5;
            sound.playHitSound();
            food_aY = screenWidth;
        }

        if (eat(food_a1X, food_a1Y)) {
            score += 25;
            sound.playHitSound();
            food_a1Y = screenWidth;
        }

        if (eat(food_sX, food_sY)) {
            score += 50;
            sound.playHitSound();
            food_sY = screenWidth;
        }

        if (eat(food_ssX, food_ssY)) {
            score += 100;
            sound.playHitSound();
            food_ssY = screenWidth;
        }
        //toxic
        if (eat(toxic_aX, toxic_aY)) {
            toxic_aY = screenWidth;
            score /= 2;
        }
        if (eat(toxic_sX, toxic_sY)) {
            toxic_sY = screenWidth;
            nyawa -= 1;
        }

        if(score <0 ){
            score = 0;
        }

        if(nyawa < 0) {
            nyawa = 0;
            //stop timer!!
            timer.cancel();
            timer = null;
            sound.playOverSound();
        }
    }

    public void character() {
        mainCharacter.setX(mainCharacterX);
        mainCharacter.setY(mainCharacterY);

        //move character
        if (action_flag) {
            //releasing
            mainCharacterX += mainCharacterSpeed;
        } else {
            //touching
            mainCharacterX -= mainCharacterSpeed;
        }

        if (action_jump) {
            mainCharacterY -= mainCharacterSpeed;
        }else{
            mainCharacterY += mainCharacterSpeed;
        }

        //chaeck character position
        if (mainCharacterX < 0) mainCharacterX = 0;
        if (screenWidth - mainCharacter.getWidth() < mainCharacterX) mainCharacterX = screenWidth - mainCharacter.getWidth();


        //jump
        if (mainCharacterY < screenHeight - mainCharacter.getHeight()*2) mainCharacterY = screenHeight - mainCharacter.getHeight();
        if (screenHeight - mainCharacter.getHeight() < mainCharacterY)  mainCharacterY = screenHeight - mainCharacter.getHeight();


        mainCharacter.setX(mainCharacterX);
        mainCharacter.setY(mainCharacterY);
    }

    public boolean eat(float x, float y) {
        return mainCharacterX < x && x < (mainCharacterX + mainCharacter.getWidth()) && mainCharacterY < y && y < (mainCharacterY + mainCharacter.getHeight());
    }

    public void gameOver() {
        if(timer == null) {
            backSound.stop();
            Intent intent = new Intent(getApplicationContext(), result.class);
            intent.putExtra("SCORE", score);
            startActivity(intent);
            finish();
        }
    }

    public boolean onTouchEvent (MotionEvent event){
        this.gDetector.onTouchEvent(event);
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            action_flag = true;
        }else if(event.getAction() == MotionEvent.ACTION_UP){
            action_flag = false;
        }
        return super.onTouchEvent(event);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_BACK:
                    return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {

        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent motionEvent) {
        if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
            action_jump = true;
        } else if (motionEvent.getAction() == MotionEvent.ACTION_UP){
            action_jump = false;
        }else if (motionEvent.getAction() == MotionEvent.ACTION_UP){
            action_jump = true;
        }
        return false;
    }


    @Override
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
            action_jump = true;
        } else if (motionEvent.getAction() == MotionEvent.ACTION_UP){
            action_jump = false;
        }
        return true;
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {

        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {

        return true;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }
}